<!DOCTYPE html>
<html ng-app="navigrails-app">

<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <link rel="stylesheet" type="text/css" href="css/angular-chart.css"/>
    <link rel="stylesheet" type="text/css" href="css/angular-ui-notification.min.css"/>
    <title>Welcome to Navigrails!</title>
</head>

<body ng-controller="MainController as mainCtrl">

    <header>
        <h1 class="text-center">Welcome to Navigrails!</h1>
        <h2 class="text-center">--traffic-aware navigation service--</h2>
    </header>

    <div class="row">
        <div class="col-md-3">
            <div id="searchPanel" class="panel panel-primary">
                <div class="panel-heading">Navigation menu</div>
                <div class="panel-body">
                    <div class="row searchElement">
                        <navigrails-search></navigrails-search>
                    </div>
                    <div class="row searchElement">
                        <navigrails-stats></navigrails-stats>
                    </div>
                </div>
            </div>
            <div id="trafficControlPanel" class="panel panel-primary">
                <div class="panel-heading">Traffic management menu</div>
                <div class="panel-body">
                    <div class="row searchElement">
                        <navigrails-traffic></navigrails-traffic>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div id="navigationPanel" class="panel panel-info">
                <div class="panel-heading">Map</div>
                <div class="panel-body">
                    <span us-spinner="{radius:30, width:8, length:16}" spinner-key="route-spinner"></span>

                    <ui-gmap-google-map id="map-canvas" center='mapSettings.center' zoom='mapSettings.zoom' control="mapSettings.control">
                        <ui-gmap-polyline ng-repeat="leg in routePolylines"
                                          path="leg.path" stroke="leg.stroke"
                                          visible="leg.visible" geodesic="leg.geodesic"
                                          editable="leg.editable"
                                          draggable="leg.draggable" icons="leg.icons">
                        </ui-gmap-polyline>
                        <ui-gmap-marker coords="startMarker.coords" options="startMarker.options"
                                        events="startMarker.events" idkey="startMarker.id"
                                        icon="{url:'http://maps.google.com/mapfiles/ms/icons/green-dot.png'}">
                        </ui-gmap-marker>
                        <ui-gmap-marker coords="endMarker.coords" options="endMarker.options"
                                        events="endMarker.events" idkey="endMarker.id">
                        </ui-gmap-marker>
                        <ui-gmap-polyline ng-repeat="way in selectedWays"
                                          path="way.path" stroke="way.stroke"
                                          visible="way.visible" geodesic="way.geodesic"
                                          editable="way.editable"
                                          draggable="way.draggable" icons="way.icons">
                        </ui-gmap-polyline>
                    </ui-gmap-google-map>
                    <div id="legend"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>
    <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script type="application/javascript" src="https://rawgit.com/angular-ui/angular-google-maps/2.1.1/dist/angular-google-maps.min.js"></script>
    <script type="text/javascript" src="http://fgnass.github.io/spin.js/spin.min.js"></script>
    <script type="application/javascript" src="js/angular-spinner.min.js"></script>
    <script type="application/javascript" src="js/Chart.min.js"></script>
    <script type="application/javascript" src="js/angular-chart.min.js"></script>
    <script type="application/javascript" src="js/angular-ui-notification.min.js"></script>
    <g:javascript library="navigrails-traffic"></g:javascript>
    <g:javascript library="navigrails-search"></g:javascript>
    <g:javascript library="navigrails"></g:javascript>
</body>

</html>