package navigrails.model

class OsmWay implements Way {

    Long id

    List<Long> nodeIds

    List<List<Double>> nodeLocs

    //List relationIds

    Boolean oneway

    Integer highwayType

    Double averageSpeed

    List<Double> speedModel

    OsmWay(OsmWay way) {
        this.id = way.id
        this.nodeIds = way.nodeIds
        //this.keywords = way.keywords
        //this.relationIds = way.relationIds
    }

    @Override
    Long id() {
        return id
    }

    @Override
    List nodes() {
        return nodeIds
    }

    @Override
    List keywords() {
        return null
    }

    @Override
    List relations() {
        return null
    }

    @Override
    Double averageSpeed() {
        return averageSpeed
    }

    @Override
    void setAverageSpeed(Double speed) {
        averageSpeed = speed
    }

    @Override
    double[] speedModel() {
        return speedModel
    }

    @Override
    Integer highwayType() {
        return highwayType
    }

    @Override
    Boolean oneway() {
        return oneway
    }
    static constraints = {
    }

    static mapping = {
        collection "ways"
        nodeIds     attr:"nd"
        nodeLocs    attr:"loc"
        //keywords    attr:"tg"
        //relationIds attr:"relations"
        oneway      attr:"oneway"
        highwayType attr:"highwayType"
    }

    @Override
    public String toString() {
        return "OsmWay{" +
                "id=" + id +
                '}';
    }
}
