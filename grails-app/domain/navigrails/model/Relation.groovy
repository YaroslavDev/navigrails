package navigrails.model

class Relation {

    Long id

    List members

    static constraints = {
    }

    static mapping = {
        collection "relations"
        members    attr:"mm"
    }

    @Override
    public String toString() {
        return "Relation{" +
                "id=" + id +
                '}';
    }
}
