package navigrails.model

class OsmNode implements Node {

    Long id

    ArrayList<Double> location

    List keywords

    ArrayList<Long> wayIds

    Boolean roadflag

    //List relationIds

    @Override
    Long id() {
        return id
    }

    @Override
    Double lat() {
        return location[0]
    }

    @Override
    Double lon() {
        return location[1]
    }

    @Override
    List keywords() {
        return keywords
    }

    @Override
    List ways() {
        return wayIds
    }

    @Override
    List relations() {
        return null//relationIds
    }

    @Override
    Boolean roadflag() {
        return roadflag
    }

    static constraints = {
    }

    static mapping = {
        collection "nodes"
        location    attr:"loc"
        keywords    attr:"tg"
        wayIds      attr:"ways"
        //relationIds attr:"relations"
        location    geoIndex:'2dsphere'
    }

    @Override
    public String toString() {
        return "{ id : $id, loc: $location }";
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        OsmNode osmNode = (OsmNode) o

        if (id != osmNode.id) return false

        return true
    }

    int hashCode() {
        return id.hashCode()
    }
}
