import org.springframework.beans.factory.annotation.Autowired

class BootStrap {

    def nodeService

    def wayService

    @Autowired
    def geocodingService

    def init = { servletContext ->
        geocodingService.initialize()

        nodeService.initialize()

        wayService.initialize()
    }
    def destroy = {
    }
}
