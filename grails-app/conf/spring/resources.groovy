import navigrails.services.AStarSearch
import navigrails.services.EstimateTimeFunction
import navigrails.util.SphericalDistanceCalculator
import navigrails.util.StaticTimeService
import navigrails.services.TravelTimeFunction

// Place your Spring DSL code here
beans = {
    calculator(SphericalDistanceCalculator) { bean ->
        bean.scope = "singleton"
    }

    search(AStarSearch)

    travelTimeFunction(TravelTimeFunction)

    heuristicFunction(EstimateTimeFunction)

    timeService(StaticTimeService, 7.5) { bean ->
        bean.scope = "singleton"
    }
}
