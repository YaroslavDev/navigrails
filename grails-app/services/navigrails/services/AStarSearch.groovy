package navigrails.services

import com.google.common.collect.MinMaxPriorityQueue
import groovy.transform.CompileStatic
import navigrails.model.Node
import navigrails.model.Way
import navigrails.util.NodeService
import navigrails.util.NodeWrapper
import navigrails.util.TimeService
import navigrails.util.WayService
import org.springframework.beans.factory.annotation.Autowired

import static java.lang.Math.abs

@CompileStatic
class AStarSearch extends GraphSearch {
    Integer epsilon = 3 //seconds

    Integer maximumFringeSize = 200

    @Autowired
    TravelTimeFunction travelTimeFunction

    @Autowired
    TimeService timeService

    @Autowired
    NodeService nodeService

    @Autowired
    WayService wayService

    class NodeComparator implements Comparator<NodeWrapper> {

        HashMap<Long, Double> travelTime

        NodeWrapper destination

        @Override
        int compare(NodeWrapper w1, NodeWrapper w2) {
            Double time1 = travelTime.get(w1.id())
            Double heuristic1 = heuristicFunction.apply(timeService.currentTime + timeService.toHours(time1), w1.node, destination)
            Double totalTime1 = time1 + heuristic1

            Double time2 = travelTime.get(w2.id())
            Double heuristic2 = heuristicFunction.apply(timeService.currentTime + timeService.toHours(time2), w2.node, destination)
            Double totalTime2 = time2 + heuristic2

//            if (heuristic1 < heuristic2) {
//                return -1
//            } else if (heuristic1 > heuristic2) {
//                return 1
//            } else {
//                return 0
//            }

            if (abs(totalTime1 - totalTime2) < epsilon) {
                if (heuristic1 < heuristic2) {
                    return -1
                } else if (heuristic1 > heuristic2) {
                    return 1
                } else {
                    return 0
                }
            } else if (totalTime1 < totalTime2) {
                return -1
            } else if (totalTime1 > totalTime2) {
                return 1
            } else {
                return 0    // should never reach this code
            }
        }
    }

    @Override
    NodeWrapper computeOptimalPath(NodeWrapper origin, NodeWrapper destination) {
        Integer maxFringeSize = 0
        Integer exploredSetSize = 0

        HashMap<Long, Double> travelTime = new HashMap<Long, Double>()

        NodeComparator nodeComparator = new NodeComparator()
        nodeComparator.travelTime = travelTime
        nodeComparator.destination = destination
        MinMaxPriorityQueue<NodeWrapper> fringe = MinMaxPriorityQueue.orderedBy(nodeComparator).maximumSize(maximumFringeSize).create()

        HashSet<Long> explored = new HashSet<Long>()
        fringe.add(origin)
        travelTime.put(origin.id(), (Double) 0.0)
        while (!fringe.isEmpty()) {
            if (fringe.size() > maxFringeSize) {
                maxFringeSize = fringe.size()
            }
            if (explored.size() > exploredSetSize) {
                exploredSetSize = explored.size()
            }
            NodeWrapper current = fringe.poll()
            explored.add(current.id())

            if (current.id() == destination.id()) {
                destination.setPrev(current.prev(), current.speedToPrev)
                println("Max fringe size = " + maxFringeSize)
                println("Max explored set size = " + exploredSetSize)
                return current
            }

            current.ways().collect{wayService.findOne(it)}.findAll{it.highwayType() <= 16}.each {
                addNodesToFringe(true, current, it, travelTime, explored, fringe)
                if (!it.oneway()) {
                    addNodesToFringe(false, current, it, travelTime, explored, fringe)
                }
            }
        }

        return null
    }

    def addNodesToFringe(Boolean forward,
                         NodeWrapper current, Way way,
                         HashMap<Long, Double> travelTime, HashSet<Long> explored, MinMaxPriorityQueue<NodeWrapper> fringe) {
        NodeWrapper prevW = current

        Integer currentIndex = way.nodes().indexOf(current.id())
        Integer nodesLength = way.nodes().size()
        List<Long> nodes = way.nodes()

        if (forward) {
            for (Integer i = currentIndex + 1; i < nodesLength; i++) {
                prevW = addNodeToFringe(nodes[i], prevW, current, way, travelTime, explored, fringe)
            }
        } else {
            for (Integer i = currentIndex - 1; i >= 0; i--) {
                prevW = addNodeToFringe(nodes[i], prevW, current, way, travelTime, explored, fringe)
            }
        }
    }

    NodeWrapper addNodeToFringe(Long nodeId,
                        NodeWrapper prevW, NodeWrapper current, Way way,
                        HashMap<Long, Double> travelTime, HashSet<Long> explored, MinMaxPriorityQueue<NodeWrapper> fringe) {
        if (!explored.contains(nodeId)) {
            Node next = nodeService.findOne(nodeId)

            if (travelTime[nodeId] == null) {
                travelTime[nodeId] = Double.POSITIVE_INFINITY
            }

            Double currentTravelTime = travelTime[current.id()]
            List<Double> travelStats =
            travelTimeFunction.apply(timeService.currentTime + timeService.toHours(currentTravelTime), way, current, next)
            Double avgSpeed = travelStats[2]
            Double nextTravelTime = travelStats[0]
            NodeWrapper nextW = new NodeWrapper(node: next).setPrev(prevW, avgSpeed)

            Double nextTotalTravelTime = currentTravelTime + nextTravelTime

            if (!fringe.contains(nextW) || travelTime[nodeId] > nextTotalTravelTime) {
                travelTime[nodeId] = nextTotalTravelTime
                if (!fringe.contains(nextW)) {
                    fringe.add(nextW)
                } else {
                    fringe.remove(nextW)
                    fringe.add(nextW)
                }
            }
            return nextW
        } else
            return prevW
    }
}
