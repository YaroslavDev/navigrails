package navigrails.services

import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import com.google.maps.model.LatLng
import grails.transaction.Transactional
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value

@Transactional
class GeocodingService {
    @Value('${google.services.apiKey}')
    String apiKey

    static context = new GeoApiContext()

    LatLng geocode(String address) {
        def request = GeocodingApi.geocode(context, address)
        def resultsStart = request.await()
        def point = resultsStart.first().geometry.location
        return point
    }

    void initialize() {
        context.setApiKey(apiKey)
    }
}
