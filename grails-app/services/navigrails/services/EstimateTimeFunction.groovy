package navigrails.services

import groovy.transform.CompileStatic
import navigrails.model.Node
import navigrails.util.DistanceCalculator
import org.springframework.beans.factory.annotation.Autowired

@CompileStatic
class EstimateTimeFunction {
    @Autowired
    DistanceCalculator calculator
    Double meanSpeed = 20 // meter/second

    Double apply(Double time, Node start, Node end) {
        Double distance = calculator.distanceBetween(start, end)
        Double travelTime = (Double) distance / meanSpeed
        return travelTime
    }
}
