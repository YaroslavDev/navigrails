package navigrails.services

import navigrails.model.Node
import navigrails.model.NodeDTO
import navigrails.model.OsmNode
import navigrails.util.DistanceCalculator
import navigrails.util.NodeWrapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value

class RouteService {
    @Autowired
    GraphSearch search

    @Value('${navigation.latitude.min}')
    Double minLat

    @Value('${navigation.latitude.max}')
    Double maxLat

    @Value('${navigation.longitude.min}')
    Double minLon

    @Value('${navigation.longitude.max}')
    Double maxLon

    def computeRoute(Double startLat, Double startLon, Double endLat, Double endLon) {
        Node origin = new NodeDTO(startLat, startLon)
        NodeWrapper originW = new NodeWrapper(node: origin, prev: null)

        Node nearOrigin = findClosestRoadNode(startLat, startLon)
        NodeWrapper nearOriginW = new NodeWrapper(node: nearOrigin, prev: originW)

        def nearDestination = findClosestRoadNode(endLat, endLon)
        def nearDestinationW = new NodeWrapper(node: nearDestination, prev: null)

        def destination = new NodeDTO(endLat, endLon)
        def destinationW = new NodeWrapper(node: destination, prev: nearDestinationW)

        def nearDst = search.computeOptimalPath(nearOriginW, nearDestinationW)
        if (nearDst == null) {
            destinationW.setPrev(nearDst)
        }

        def path = search.buildPath(destinationW)

        return path
    }

    Node findClosestRoadNode(Double lat, Double lon) {
        Node node = OsmNode.findByLocationNearAndRoadflag([lat, lon], true)

        return node
    }

    def validateCoordinates(Double startLat, Double startLon, Double endLat, Double endLon) {
        return numberInBounds(startLat, minLat, maxLat) && numberInBounds(startLon, minLon, maxLon) &&
                numberInBounds(endLat, minLat, maxLat) && numberInBounds(endLon, minLon, maxLon)
    }

    def numberInBounds(Double number, Double min, Double max) {
        return number > min && number < max;
    }
}
