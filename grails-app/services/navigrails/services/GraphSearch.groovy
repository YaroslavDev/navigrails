package navigrails.services

import navigrails.util.NodeWrapper
import org.springframework.beans.factory.annotation.Autowired

abstract class GraphSearch {
    @Autowired
    protected EstimateTimeFunction heuristicFunction

    abstract NodeWrapper computeOptimalPath(NodeWrapper origin, NodeWrapper destination);

    def buildPath(NodeWrapper target) {
        NodeWrapper current = target
        NodeWrapper previous = target.prev()

        List path = []

        Double totalDistance = 0.0, totalTravelTime = 0.0, totalAverageSpeed = 0.0, numLegs = 0.0

        while (previous != null) {
            path += [
                        distance: current.distanceToPrev,
                        travelTime: current.timeToPrev,
                        averageSpeed: current.speedToPrev,
                        start: [ previous.lat(), previous.lon() ],
                        end: [ current.lat(), current.lon() ]
                    ]
            totalDistance += current.distanceToPrev
            totalTravelTime += current.timeToPrev
            numLegs += 1.0
            current = previous
            previous = previous.prev()
        }
        if (totalTravelTime != 0.0) {
            totalAverageSpeed = totalDistance / totalTravelTime
        } else {
            totalAverageSpeed = 0.0
        }

        def route = [
            distance: totalDistance,
            travelTime: totalTravelTime,
            averageSpeed: totalAverageSpeed,
            legs: path.reverse()
        ]

        return route
    }
}