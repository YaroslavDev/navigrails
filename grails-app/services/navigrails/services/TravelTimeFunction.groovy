package navigrails.services

import groovy.transform.CompileStatic
import navigrails.model.Node
import navigrails.model.Way
import navigrails.util.DistanceCalculator
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction
import org.springframework.beans.factory.annotation.Autowired

@CompileStatic
class TravelTimeFunction {
    @Autowired
    DistanceCalculator calculator

    Double defaultAvgSpeed = 8 // m/s

    List<Double> apply(Double currentTime, Way way, Node start, Node end) {
        double maxHours = 24.0
        currentTime %= maxHours
        if (currentTime < 0.0) {
            currentTime = maxHours + currentTime
        }
        Double nextDistance = calculator.distanceBetween(start, end)
        Double avgSpeed = way.averageSpeed()    // in meter / second
        if (way.speedModel()) {
            PolynomialFunction model = new PolynomialFunction(way.speedModel())
            avgSpeed = model.value(currentTime)
        }
        if (avgSpeed == null) {
            avgSpeed = defaultAvgSpeed
        }
        Double travelTime = (Double) nextDistance / avgSpeed // in seconds
        return [travelTime, nextDistance, avgSpeed]
    }
}
