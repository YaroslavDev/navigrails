package navigrails.util

import navigrails.model.Node
import org.springframework.beans.factory.annotation.Autowired


class NodeWrapper implements Node {
    final static DistanceCalculator calculator = new SphericalDistanceCalculator()

    final static Double meanPedestrianSpeed = 1.37

    Node node
    NodeWrapper prev
    Double distanceToPrev
    Double timeToPrev
    Double speedToPrev

    Node node() {
        return node
    }

    NodeWrapper prev() {
        return prev
    }

    NodeWrapper setPrev(NodeWrapper _prev, Double avgSpeed = meanPedestrianSpeed) {
        if (_prev != null) {
            distanceToPrev = calculator.distanceBetween(this, _prev)
            timeToPrev = distanceToPrev / avgSpeed
            speedToPrev = avgSpeed
        } else {
            distanceToPrev = timeToPrev = speedToPrev = 0.0
        }
        prev = _prev
        return this
    }

    @Override
    List keywords() {
        return node.keywords()
    }

    @Override
    Boolean roadflag() {
        return node.roadflag()
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        NodeWrapper that = (NodeWrapper) o

        if (node != that.node) return false

        return true
    }

    int hashCode() {
        return node.hashCode()
    }

    @Override
    Long id() {
        return node.id()
    }

    @Override
    Double lat() {
        return node.lat()
    }

    @Override
    Double lon() {
        return node.lon()
    }

    @Override
    List<Long> ways() {
        return node.ways()
    }

    @Override
    List relations() {
        return node.relations()
    }
}
