package navigrails.util

interface TimeService {

    Double getCurrentTime();

    void setCurrentTime(Double time);

    Double toHours(Double timeInSecs);

    Double toSecs(Double timeInHours);
}
