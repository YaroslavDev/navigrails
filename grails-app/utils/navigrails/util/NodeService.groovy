package navigrails.util

import com.google.common.collect.Maps
import navigrails.model.OsmNode

class NodeService {

    HashMap<Long, OsmNode> nodeStore

    OsmNode findOne(Long id) {
        OsmNode node = null
        if (nodeStore != null)
            node = nodeStore.get(id)
        else
            node = OsmNode.get(id)
        return node
    }

    def initialize() {
        nodeStore = Maps.newHashMapWithExpectedSize(OsmNode.count)
        OsmNode.findAll().each {
            nodeStore.put(it.id, it)
        }
    }
}
