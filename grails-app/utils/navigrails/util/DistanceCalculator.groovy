package navigrails.util

import navigrails.model.Node

interface DistanceCalculator {

    Double distanceBetween(Node n1, Node n2);

    Boolean closeEnough(Node n1, Node n2, Double epsilon);
}