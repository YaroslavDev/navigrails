package navigrails.util

import com.google.common.collect.Maps
import navigrails.model.OsmWay
import navigrails.model.Way

class WayService {

    HashMap<Long, OsmWay> wayStore

    Way findOne(Long id) {
        Way way = null
        if(wayStore != null)
            way = wayStore.get(id)
        else
            way = (OsmWay) OsmWay.get(id)
        return way
    }

    void saveWay(OsmWay way) {
        if (wayStore != null) {
            wayStore.put(way.id(), way)
        }
        way.save()
    }

    def initialize() {
        wayStore = Maps.newHashMapWithExpectedSize(OsmWay.count)
        OsmWay.findAll().each {
            wayStore.put(it.id, it)
        }
    }
}
