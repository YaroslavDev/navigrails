package navigrails.util

import navigrails.model.Node

import static Math.sin
import static Math.cos
import static Math.toRadians
import static java.lang.Math.asin
import static java.lang.Math.sqrt

class SphericalDistanceCalculator implements DistanceCalculator {

    @Override
    Double distanceBetween(Node n1, Node n2) {

        Double R = 6371000.0 //metres
        Double phi1 = toRadians(n1.lat())
        Double phi2 = toRadians(n2.lat())

        Double deltaPhi = toRadians(n2.lat() - n1.lat())
        Double deltaLambda = toRadians(n2.lon() - n1.lon())

        Double a = sin(deltaPhi/2.0) * sin(deltaPhi/2.0) +
                cos(phi1) * cos(phi2) * sin(deltaLambda/2.0) * sin(deltaLambda/2.0)
        Double c = 2.0 * asin(sqrt(a))

        Double distance = R * c

        return distance
    }

    @Override
    Boolean closeEnough(Node n1, Node n2, Double epsilon) {
        return distanceBetween(n1, n2) < epsilon
    }
}
