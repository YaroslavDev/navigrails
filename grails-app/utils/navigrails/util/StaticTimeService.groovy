package navigrails.util

import groovy.transform.CompileStatic

@CompileStatic
class StaticTimeService implements TimeService {

    private Double time
    private Double secsInHours = (Double) 3600.0

    public StaticTimeService(Double _time = 0.0) {
        time = _time
    }

    @Override
    Double getCurrentTime() {
        return time
    }

    @Override
    void setCurrentTime(Double _time) {
        Double maxHours = (Double) 24.0
        time = _time % maxHours
        if (time < 0.0) {
            time = maxHours + time
        }
    }

    @Override
    Double toHours(Double timeInSecs) {
        return (Double) timeInSecs / secsInHours
    }

    @Override
    Double toSecs(Double timeInHours) {
        return (Double) timeInHours * secsInHours;
    }
}
