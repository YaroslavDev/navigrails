package navigrails.controllers

import com.google.maps.model.LatLng
import groovy.transform.CompileStatic
import navigrails.model.OsmNode
import navigrails.model.OsmWay
import navigrails.model.Way
import navigrails.services.GeocodingService
import navigrails.services.RouteService
import navigrails.util.NodeService
import navigrails.util.StaticTimeService
import navigrails.util.WayService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["navigrails.services"])
class NavigationController {

    static responseFormats = ['json']

    String localization = 'Chisinau'

    @Autowired
    RouteService routeService

    @Autowired
    StaticTimeService timeService

    @Autowired
    GeocodingService geocodingService

    @Autowired
    WayService wayService

    @Autowired
    NodeService nodeService

    def index() {
        render "Use REST API"
    }

    def route(Double startLat, Double startLon, Double endLat, Double endLon) {
        def timeout = 8
        Object path = null
        def status = "Success"
        if (routeService.validateCoordinates(startLat, startLon, endLat, endLon)) {
            path = routeService.computeRoute(startLat, startLon, endLat, endLon)
        } else {
            status = "Error: Route outside boundaries!"
        }
//        ExecutorService executor = Executors.newSingleThreadExecutor()
//        Future<Object> pathFuture = executor.submit(new Callable<Object>() {
//            @Override
//            Object call() throws Exception {
//                return routeService.computeRoute(startLat, startLon, endLat, endLon)
//            }
//        })
//        try {
//            path = pathFuture.get(timeout, TimeUnit.SECONDS)
//        } catch (TimeoutException ex) {
//            System.out.println("Route finding process took longer than $timeout seconds - aborting!")
//        }
        render(contentType: 'application/json') { [
                'systemTime' : timeService.getCurrentTime(),
                'route' : path,
                'status': status
        ]};
    }

    def path(String startAddress, String endAddress) {
        LatLng startPoint = geocodingService.geocode(localization + ' ' + startAddress)
        LatLng endPoint = geocodingService.geocode(localization + ' ' + endAddress)

        route(startPoint.lat, startPoint.lng, endPoint.lat, endPoint.lng)
    }

    def internalTime() {
        def jsonObject = request.JSON
        Double time = (Double) jsonObject["time"]
        timeService.setCurrentTime(time)
        Double actualTime = timeService.getCurrentTime()
        render(contentType: 'application/json') {[
                'systemTime': actualTime
        ]};
    }

    def wayInfo(Double lat, Double lon) {
        OsmNode node = routeService.findClosestRoadNode(lat, lon)
        Way way = wayService.findOne(node.ways().first())

        render(contentType: 'application/json') {[
                'way': way
        ]};
    }

    def waySpeed(Long wayId, Double avgSpeed) {
        Way way = (OsmWay) wayService.findOne(wayId);
        way.setAverageSpeed(avgSpeed);
        wayService.saveWay(way);
        render(contentType: 'application/json') {[
                'status': 'ok'
        ]};
    }
}
