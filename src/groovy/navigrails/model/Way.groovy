package navigrails.model


trait Way {

    abstract Long id();

    abstract List nodes();

    abstract List keywords();

    abstract List relations();

    abstract Double averageSpeed();

    abstract void setAverageSpeed(Double speed);

    abstract double[] speedModel();

    abstract Boolean oneway();

    abstract Integer highwayType();
}