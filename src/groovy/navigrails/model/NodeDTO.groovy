package navigrails.model


class NodeDTO implements Node {

    private Double lat
    private Double lon

    public NodeDTO(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    Long id() {
        return null
    }

    @Override
    Double lat() {
        return lat
    }

    @Override
    Double lon() {
        return lon
    }

    @Override
    List keywords() {
        return null
    }

    @Override
    List ways() {
        return null
    }

    @Override
    List relations() {
        return null
    }

    @Override
    Boolean roadflag() {
        return null
    }
}
