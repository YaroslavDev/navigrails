package navigrails.model

trait Node {

    abstract Long id();

    abstract Double lat();

    abstract Double lon();

    abstract List keywords();

    abstract List<Long> ways();

    abstract List relations();

    abstract Boolean roadflag();
}