package navigrails

import navigrails.model.NodeDTO
import navigrails.util.SphericalDistanceCalculator
import spock.lang.Specification
import static Math.abs

class SphericalDistanceCalculatorSpec extends Specification{

    def "distance between Home and Endava"() {
        setup:
            def calculator = new SphericalDistanceCalculator()
            def home = new NodeDTO(47.029153, 28.788557)
            def endava = new NodeDTO(47.024523, 28.820851)
            def distance = calculator.distanceBetween(home, endava)
            def trueDistance = 2501
            def epsilon = 3
            println(distance)

        expect:
            abs(distance - trueDistance) < epsilon
    }

    def "distance between Home and Masarikova Kolej"() {
        setup:
            def calculator = new SphericalDistanceCalculator()
            def home = new NodeDTO(47.029153, 28.788557)
            def kolej = new NodeDTO(50.100795, 14.387181)
            def distance = calculator.distanceBetween(home, kolej)
            def trueDistance = 1111330
            def epsilon = 3
            println(distance)

        expect:
            abs(distance - trueDistance) < epsilon
    }

    def "distances in Chisinau"() {
        setup:
            def calculator = new SphericalDistanceCalculator()
            def p1 = new NodeDTO(47.0185804, 28.8229752)
            def p2 = new NodeDTO(47.018624, 28.826715)
            def distance = calculator.distanceBetween(p1, p2)
            def trueDistance = 283
            def epsilon = 3
            println(distance)

        expect:
            abs(distance - trueDistance) < epsilon
    }
}
