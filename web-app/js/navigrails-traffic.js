(function() {
    var app = angular.module('navigrails-traffic', [ 'ui-notification' ]);

    app.directive('navigrailsTraffic', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/navigrails-traffic.html',
            controller: [ '$scope', '$http', 'Notification', function($scope, $http, Notification) {
                this.time = 0.0;
                this.wayId = 0;
                this.averageSpeed = 0;
                this.modelAvailable = false;
                this.xdata = [];
                this.ydata = [];
                var ctrl = this;

                $scope.$on('routeComputed', function(event, routeObject) {
                   ctrl.time = routeObject.systemTime;
                });

                this.setSystemTime = function() {
                    $http.post('/Navigrails/navigation/internalTime', {time: this.time})
                        .success(function(timeObject) {
                            $scope.$emit('timeSet', timeObject);
                            Notification.info({message: "Time set to {0}".format(timeObject.systemTime), delay: 1000});
                        });
                };

                this.setWayAverageSpeed = function() {
                    if (this.wayId == null) {
                        Notification.error({message: "Way ID is empty", delay: 2000});
                        return;
                    }
                    if (this.averageSpeed == null) {
                        Notification.error({message: "Way ID is empty", delay: 2000});
                        return;
                    }
                    $http.post("/Navigrails/navigation/waySpeed?wayId={0}&avgSpeed={1}".format(ctrl.wayId, ctrl.averageSpeed))
                        .success(function(statusObject) {
                           if (statusObject.status === "ok") {
                               Notification.success({message: "Way speed updated", delay: 1000});
                           }
                        });
                };

                $scope.$on('pointClicked', function(event, point) {
                    $http.get("/Navigrails/navigation/wayInfo?lat={0}&lon={1}".format(point.A, point.F))
                        .success(function(wayObject) {
                            var way = wayObject.way;
                            ctrl.wayId = way.id;
                            if (way.averageSpeed) {
                                ctrl.averageSpeed = Math.floor(way.averageSpeed * 100.0) / 100.0;
                            }
                            if (way.speedModel) {
                                var model = way.speedModel.reverse();
                                var xdata = [];
                                var ydata = [];
                                for (var t = 0; t < 24; t+=2) {
                                    xdata.push(t);
                                    var y = 0;
                                    for (var i = 0; i < model.length; i++) {
                                        y = (y * t) + model[i];
                                    }
                                    var kmPerHour = 3.6 * y.toFixed(2);
                                    ydata.push(kmPerHour);
                                }
                                ctrl.xdata = xdata;
                                ctrl.ydata = [ydata];
                                ctrl.modelAvailable = true;
                            } else {
                                ctrl.modelAvailable = false;
                                ctrl.xdata = [];
                                ctrl.ydata = [];
                            }
                            if (way.nodeLocs) {
                                var path = [];
                                way.nodeLocs.forEach(function(point) {
                                    path.push({latitude: point[0], longitude: point[1]})
                                });
                                $scope.selectedWays = [{
                                    id: 1000,
                                    path: path,
                                    stroke: {
                                        color: '#0000FF',
                                        opacity: 0.25
                                    },
                                    visible: true,
                                    icons: [{
                                        offset: '25px',
                                        repeat: '100px'
                                    }]
                                }];
                            }
                        });
                });
            }],
            controllerAs: 'trafficCtrl'
        };
    });

    app.directive('ngEnter', function() {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        }
    });
})();