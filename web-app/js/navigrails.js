(function() {
    var app = angular.module('navigrails-app', [ 'navigrails-traffic', 'navigrails-search', 'uiGmapgoogle-maps', 'angularSpinner', 'chart.js', 'ui-notification' ]);

    app.config(['uiGmapGoogleMapApiProvider', function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            v: '3.20',
            libraries: 'weather,geometry,visualization'
        });
    }]);

    function copy(obj) {
        return $.extend(true,  {}, obj)
    }

    var routeStartPoint = { latitude: 47.021803032767174, longitude: 28.803743431060866 };
    var routeEndPoint = { latitude: 47.022186499139806, longitude: 28.809558311378396 };

    String.prototype.format = function () {
        var args = [].slice.call(arguments);
        return this.replace(/(\{\d+\})/g, function (a){
            return args[+(a.substr(1,a.length-2))||0];
        });
    };

    app.controller('MainController', [ '$scope', '$http', 'usSpinnerService', '$q', 'uiGmapGoogleMapApi', 'uiGmapIsReady', 'Notification',
        function($scope, $http, usSpinnerService, $q, uiGmapGoogleMapApi, uiGmapIsReady, Notification) {
        $scope.mapSettings = { center: routeStartPoint, zoom: 15, control: {} };

        uiGmapGoogleMapApi.then(function(maps) {
            uiGmapIsReady.promise().then(function(maps) {
                var map = $scope.mapSettings.control.getGMap();
                $scope.map = map;
                var legend = document.getElementById("legend");
                map.controls[google.maps.ControlPosition.LEFT_TOP].push(legend);
                var startPointLegend = document.createElement('div');
                startPointLegend.innerHTML = '<img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png"> Start point';
                legend.appendChild(startPointLegend);
                var endPointLegend = document.createElement('div');
                endPointLegend.innerHTML = '<img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png"> End point';
                legend.appendChild(endPointLegend);
                var upperLimit = document.createElement('div');
                upperLimit.innerHTML = '70+ km/h';
                var speedScale = document.createElement('div');
                speedScale.innerHTML = '<img src="images/speedScaleIcon.png">';
                upperLimit.appendChild(speedScale);
                var lowerLimit = document.createElement('div');
                lowerLimit.innerHTML = '0 km/h';
                speedScale.appendChild(lowerLimit);
                legend.appendChild(upperLimit);

                google.maps.event.addListener(map, 'click', function(event) {
                    $scope.$emit('pointClicked', event.latLng);
                });
            });
        });

        $scope.routePolylines = [];

        $scope.selectedWays = [];

        $scope.startMarker = {
            id: 0,
            coords: copy(routeStartPoint),
            options: {
                draggable: true
            },
            events: {
                dragend: function(marker, eventName, args) {
                    $scope.$emit('startPointDefined', marker.getPosition());
                }
            }
        };

        $scope.endMarker = {
            id: 1,
            coords: copy(routeEndPoint),
            options: {
                draggable: true
            },
            events: {
                dragend: function(marker, eventName, args) {
                    $scope.$emit('endPointDefined', marker.getPosition());
                }
            }
        };

        $scope.$on('requestCancelled', function(event) {
            console.log("requestCancelled");
            usSpinnerService.stop('route-spinner');
            if ($scope.canceler) {
                $scope.canceler.resolve();
            }
        });

        $scope.$on('computeRoute', function (event, type, startPoint, endPoint) {
            usSpinnerService.spin('route-spinner');

            var queryString = "/Navigrails/navigation/";
            if (type == "byCoordinate") {
                queryString += "route?startLat={0}&startLon={1}&endLat={2}&endLon={3}".format(startPoint.latitude, startPoint.longitude, endPoint.latitude, endPoint.longitude);
            } else if (type == "byAddress") {
                var startAddress = startPoint.match(/\S+/g).join("+");
                var endAddress = endPoint.match(/\S+/g).join("+");
                queryString += "path?startAddress={0}&endAddress={1}".format(startAddress, endAddress);
            } else {
                console.log("Unknown event");
            }
            console.log(queryString);


            $scope.canceler = $q.defer();
            $http.get(queryString, {timeout: $scope.canceler.promise}).success(function (routeObject) {
                usSpinnerService.stop('route-spinner');

                if (routeObject.route && routeObject.route.legs.length != 0) {
                    var n = routeObject.route.legs.length;
                    var routePolylines = [];
                    var bounds = new google.maps.LatLngBounds();
                    var center = {latitude: 0.0, longitude: 0.0};
                    var dashSymbol = {
                        path: 'M 0,-1 0,1',
                        strokeOpacity: 1,
                        scale: 4
                    };
                    var legs = routeObject.route.legs;
                    for (var i = 0; i < legs.length; i++) {
                        var speedRgb = speedToRgb(legs[i].averageSpeed);
                        var speedHex = rgbToHex(speedRgb[0], speedRgb[1], speedRgb[2]);
                        var polyline = {};
                        if (i == 0 || i == legs.length - 1) {
                            polyline = {
                                id: i,
                                path: [
                                    copy({latitude: legs[i].start[0], longitude: legs[i].start[1]}),
                                    copy({latitude: legs[i].end[0], longitude: legs[i].end[1]})
                                ],
                                stroke: {
                                    color: speedHex,
                                    opacity: 0.0
                                },
                                visible: true,
                                icons: [{
                                    icon: dashSymbol,
                                    offset: '0',
                                    repeat: '25px'
                                }]
                            };
                        } else {
                            polyline = {
                                id: i,
                                path: [
                                    copy({latitude: legs[i].start[0], longitude: legs[i].start[1]}),
                                    copy({latitude: legs[i].end[0], longitude: legs[i].end[1]})
                                ],
                                stroke: {
                                    color: speedHex,
                                    opacity: 1.0
                                },
                                visible: true,
                                icons: [{
                                    icon: {
                                        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
                                    },
                                    offset: '25px',
                                    repeat: '100px'
                                }]
                            };
                        }
                        var boundPoint = new google.maps.LatLng(legs[i].start[0], legs[i].start[1]);
                        bounds.extend(boundPoint);
                        center.latitude += boundPoint.lat;
                        center.longitude += boundPoint.lng;
                        routePolylines.push(polyline);
                    }
                    $scope.routePolylines = routePolylines;
                    center.latitude /= n;
                    center.longitude /= n;
                    $scope.map.setCenter(bounds.getCenter());
                    $scope.map.fitBounds(bounds);
                    var startPoint = {latitude: routeObject.route.legs[0].start[0], longitude: routeObject.route.legs[0].start[1]};
                    var endPoint = {latitude: routeObject.route.legs[n-1].end[0], longitude: routeObject.route.legs[n-1].end[1]};
                    $scope.startMarker.coords = startPoint;
                    $scope.endMarker.coords = endPoint;
                } else {
                    console.log("Path not found!");
                }
                $scope.$emit('routeComputed', routeObject);
            }).error(function() {
                usSpinnerService.stop('route-spinner');
            });
        });
    }]);

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    function speedToRgb(speed) {
        var maxSpeed = 20;
        var g = 0;
        if (speed > maxSpeed) {
            g = 255;
        } else {
            g = Math.floor(speed * 255 / maxSpeed);
        }
        return [255 - g, g, 0];
    }
})();