(function() {
    var app = angular.module('navigrails-search', [ 'ui-notification' ]);

    var routeStartPoint = { latitude: 47.021803032767174, longitude: 28.803743431060866 };
    var routeEndPoint = { latitude: 47.022186499139806, longitude: 28.809558311378396 };

    app.directive('navigrailsSearch', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/navigrails-search.html',
            controller: [ '$scope', function($scope) {

                this.startPoint = "{0}, {1}".format(routeStartPoint.latitude, routeStartPoint.longitude);
                this.endPoint = "{0}, {1}".format(routeEndPoint.latitude, routeEndPoint.longitude);

                var searchCtrl = this;
                $scope.$on('startPointDefined', function(event, startPoint) {
                    console.log('startPoint {0}'.format(startPoint));
                    searchCtrl.startPoint = "{0}, {1}".format(startPoint.lat(), startPoint.lng());
                });

                $scope.$on('endPointDefined', function(event, endPoint) {
                    searchCtrl.endPoint = "{0}, {1}".format(endPoint.lat(), endPoint.lng());
                });

                this.startAddress = "";
                this.endAddress = "";

                this.tabSelected = 1;

                this.selectTab = function(tab) {
                    this.tabSelected = tab;
                };

                this.isSelected = function(tab) {
                    return this.tabSelected == tab;
                };

                this.computeRoute = function() {
                    if (this.tabSelected == 1) {
                        var startPair = this.startPoint.split(", ");
                        var endPair = this.endPoint.split(", ");

                        var startPoint = { latitude: startPair[0], longitude: startPair[1] };
                        var endPoint = { latitude: endPair[0], longitude: endPair[1] };
                        $scope.$emit('computeRoute', 'byCoordinate', startPoint, endPoint);
                    } else if (this.tabSelected == 2) {
                        console.log(this.startAddress, this.endAddress);
                        $scope.$emit('computeRoute', 'byAddress', this.startAddress, this.endAddress);
                    }
                };

                this.cancelRequest = function() {
                    $scope.$emit('requestCancelled');
                };
            }],
            controllerAs: 'searchCtrl'
        };
    });

    app.directive('navigrailsStats', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/navigrails-stats.html',
            controller: [ '$scope', 'Notification', function($scope, Notification) {
                this.totalDistance = 0.0;
                this.totalTravelTime = 0.0;
                this.totalAverageSpeed = 0.0;

                var statsCtrl = this;
                $scope.$on('routeComputed', function(event, routeObject) {
                    var route = routeObject.route;
                    if (routeObject.status == "Success") {
                        statsCtrl.totalDistance = route.distance;
                        statsCtrl.totalTravelTime = route.travelTime;
                        statsCtrl.totalAverageSpeed = route.averageSpeed;
                        Notification.success({message: routeObject.status, delay: 1000});
                    } else {
                        Notification.error({message:  routeObject.status, delay: 2000});
                    }
                });

                $scope.$on('timeSet', function(event, timeObject) {
                    statsCtrl.systemTime = timeObject.systemTime;
                });
            }],
            controllerAs: 'statsCtrl'
        };
    });
})();